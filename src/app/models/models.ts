export interface EventModel {
  id: string;
  name: string;
  namespace: string; // ex. angular, react, vue
  certificate_template?: {
    file_format: string; // either pdf or html
    url: string; // url where we uploaded the certificate (cloudinary?)
  };
  venue?: {
    name: string;
    lat: number;
    long: number;
  };
  description?: string;
  survey_form?: string;
}

export interface ParticipantModel {
  event_id: string;
  name: string;
  email: string;
  hasRegistered?: boolean;
  hasLoggedIn?: boolean;
}
