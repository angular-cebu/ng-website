import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userDetails = {};
  constructor(private auth: AuthService) {
    const obv = combineLatest([this.auth.getUser$()]).subscribe(obs => {
      this.userDetails = obs[0];
      obv.unsubscribe();
    });
  }

  ngOnInit() {}
}
