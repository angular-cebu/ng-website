import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { MaterialModule } from 'src/app/material/material.module';
import { UserEventsComponent } from './components/user-events/user-events.component';

@NgModule({
  declarations: [ProfileComponent, UserEventsComponent],
  imports: [CommonModule, ProfileRoutingModule, MaterialModule]
})
export class ProfileModule {}
