import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit {
  constructor(
    private apiSvc: ApiServiceService,
    private formBuilder: FormBuilder
  ) {}

  eventForm = this.formBuilder.group({
    name: [Validators.required, Validators.minLength(5)],
    namespace: 'angular', // ex. angular, react, vue
    certificate_template: {
      file_format: '', // either pdf or html
      url: '' // url where we uploaded the certificate (cloudinary?)
    },
    venue: {
      name: '',
      lat: 0,
      long: 0
    },
    description: ''
  });

  ngOnInit() {}

  createEvent() {
    console.log(this.eventForm.getRawValue());
    if (this.eventForm.valid) {
      this.apiSvc.createEvent(this.eventForm.getRawValue()).subscribe(resp => {
        this.getEvents();
      });
    } else {
      console.log('Error');
    }
  }

  getEvents() {
    this.apiSvc.getAllEvents().subscribe(events => {
      console.log(events);
    });
  }
}
