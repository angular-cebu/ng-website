import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventComponent } from './event.component';
import { CreateEventComponent } from './components/create-event/create-event.component';

const routes: Routes = [
  {
    component: EventComponent,
    path: '',
    children: [
      {
        component: CreateEventComponent,
        path: '/create'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventRoutingModule {}
