import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventRoutingModule } from './event-routing.module';
import { EventComponent } from './event.component';
import { MaterialModule } from 'src/app/material/material.module';
import { CreateEventComponent } from './components/create-event/create-event.component';

@NgModule({
  declarations: [EventComponent, CreateEventComponent],
  imports: [CommonModule, EventRoutingModule, MaterialModule]
})
export class EventModule {}
