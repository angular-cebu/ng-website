import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'events',
        loadChildren: () =>
          import('./sections/event/event.module').then(m => m.EventModule)
      },
      {
        path: 'participants',
        loadChildren: () =>
          import('./sections/participants/participants.module').then(
            m => m.ParticipantsModule
          )
      },
      {
        path: '',
        redirectTo: 'events',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
