import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LatestRoutingModule } from './latest-routing.module';
import { LatestComponent } from './latest.component';
import { MaterialModule } from 'src/app/material/material.module';

@NgModule({
  declarations: [LatestComponent],
  imports: [CommonModule, LatestRoutingModule, MaterialModule]
})
export class LatestModule {}
