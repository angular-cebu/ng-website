import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage.component';
import { AuthGuardService } from 'src/app/services/auth-guard.service';

const routes: Routes = [
  {
    component: HomepageComponent,
    path: '',
    children: [
      {
        path: '',
        redirectTo: '/latest',
        pathMatch: 'full'
      },
      {
        path: 'events',
        loadChildren: () =>
          import('./sections/events/events.module').then(m => m.EventsModule)
      },
      {
        path: 'latest',
        loadChildren: () =>
          import('./sections/latest/latest.module').then(m => m.LatestModule)
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('./sections/profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'admin',
        loadChildren: () =>
          import('./sections/admin/admin.module').then(m => m.AdminModule),
        canActivateChild: [AuthGuardService]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomepageRoutingModule {}
