import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth.service';
import { combineLatest } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  userDetails = {};
  isLoading = true;
  isAdmin = false;
  constructor(private titleSvc: Title, private auth: AuthService) {
    this.titleSvc.setTitle(`${environment.app_title} : Home`);
  }

  ngOnInit() {
    this.isLoading = true;
    const obv = combineLatest([
      this.auth.getUser$(),
      this.auth.getPayload()
    ]).subscribe(obs => {
      this.userDetails = obs[0];
      if (
        (obs[1] as any).permissions.some(x => {
          return x === 'is:admin';
        })
      ) {
        this.isAdmin = true;
      }
      this.isLoading = false;
      obv.unsubscribe();
    });
  }

  logout() {
    this.auth.logout();
  }
}
