import {
  Component,
  OnInit,
  AfterViewInit,
  AfterContentInit
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterContentInit {
  constructor(
    private title: Title,
    private auth: AuthService,
    private router: Router
  ) {
    this.title.setTitle(`${environment.app_title} : Login`);
    if (window.location.search !== '') {
      this.auth.handleRedirectCallback$.subscribe(response => {
        this.isLoading = true;
        this.router.navigate([response.appState.target]);
      });
    }

    this.auth.isAuthenticated$.subscribe(isLoggedIn => {
      if (isLoggedIn) {
        this.router.navigate(['/']);
        return false;
      }
    });
  }

  isLoading = true;

  ngOnInit() {}

  ngAfterContentInit() {
    this.isLoading = false;
  }

  login() {
    this.isLoading = true;
    this.auth.login();
  }

  setup() {}
}
