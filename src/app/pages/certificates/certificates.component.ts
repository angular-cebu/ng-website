import { Component, OnInit } from '@angular/core';
// import { techxplore, anights, techxplore_cert } from './temp-data';
import { HttpClient } from '@angular/common/http';
import { CertificatesSvcService } from './certificates-svc.service';
@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.scss']
})
export class CertificatesComponent implements OnInit {
  email = '';
  api = new CertificatesSvcService(this.http);
  event = 'techxplore';
  constructor(public http: HttpClient) {}

  ngOnInit() {}

  async search() {
    // console.log('hello');
    // if (this.event === 'techxplore') {
    //   const result = techxplore.filter(i => {
    //     if (i['E-Mail you used in the Registration'] === this.email) {
    //       return i;
    //     }
    //     return null;
    //   });

    //   if (result.length !== 0) {
    //     const generated = await this.api.generate(result[0]['Full Name (to appear on your certificate)'], techxplore_cert);
    //     this.downloadfile(generated);
    //   } else {
    //     alert('Invalid email');
    //   }
    // }
  }

  downloadfile(data) {
    const blob = new Blob([data], { type: 'application/pdf' });
    const url = window.URL.createObjectURL(blob);
    window.location.href = url;
  }
}
