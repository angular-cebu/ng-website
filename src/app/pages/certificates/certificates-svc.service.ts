import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { generate } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CertificatesSvcService {
  constructor(public http: HttpClient) {}

  base = 'https://app-svc.herokuapp.com';

  async generate(name, template) {
    const url = `${this.base}/certificates/generate/pdf`;
    const data = {
      b64_template: template,
      template_config: [
        {
          page: 1,
          values: [
            {
              value: 'name',
              xPosition: 385,
              yPosition: 250,
              fontSize: 40,
              font: 'Arial',
              bold: true,
              align: 'center center',
              color: '000000'
            }
          ]
        }
      ]
    };

    return this.http.post(url, data, { responseType: 'blob' }).toPromise();
  }
}
