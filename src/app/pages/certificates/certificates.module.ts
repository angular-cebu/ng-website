import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificatesRoutingModule } from './certificates-routing.module';
import { CertificatesComponent } from './certificates.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CertificatesComponent],
  imports: [CommonModule, CertificatesRoutingModule, HttpClientModule, FormsModule]
})
export class CertificatesModule {}
