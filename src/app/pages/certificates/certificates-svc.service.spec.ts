import { TestBed } from '@angular/core/testing';

import { CertificatesSvcService } from './certificates-svc.service';

describe('CertificatesSvcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CertificatesSvcService = TestBed.get(CertificatesSvcService);
    expect(service).toBeTruthy();
  });
});
