import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SetupRoutingModule } from './setup-routing.module';
import { SetupComponent } from './setup.component';
import { MaterialModule } from 'src/app/material/material.module';

@NgModule({
  declarations: [SetupComponent],
  imports: [CommonModule, SetupRoutingModule, MaterialModule]
})
export class SetupModule {}
