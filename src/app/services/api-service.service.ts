import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventModel, ParticipantModel } from '../models/models';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  constructor(private http: HttpClient) {}

  base = '/jscebu/api';

  createEvent(event: EventModel) {
    const url = `${this.base}/events`;
    return this.http.post(url, event);
  }

  getAllEvents() {
    const url = `${this.base}/events`;
    return this.http.get(url, { params: { namespace: 'angular' } });
  }

  getEvent(eventId) {
    const url = `${this.base}/events/${eventId}`;
    return this.http.get(url);
  }

  addParticipants(participants: ParticipantModel[]) {
    const url = `${this.base}/events`;
    return this.http.post(url, participants);
  }

  // async generate(name, template) {
  //   const url = `${this.base}/certificates/generate/pdf`;
  //   const data = {
  //     b64_template: template,
  //     template_config: [
  //       {
  //         page: 1,
  //         values: [
  //           {
  //             value: 'name',
  //             xPosition: 385,
  //             yPosition: 250,
  //             fontSize: 40,
  //             font: 'Arial',
  //             bold: true,
  //             align: 'center center',
  //             color: '000000'
  //           }
  //         ]
  //       }
  //     ]
  //   };

  //   return this.http.post(url, data, { responseType: 'blob' }).toPromise();
  // }
}
