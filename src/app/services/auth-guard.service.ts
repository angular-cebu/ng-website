import { Injectable } from '@angular/core';
import { CanActivate, Router, CanActivateChild } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    this.auth.isAuthenticated$.subscribe(isLoggedIn => {
      if (!isLoggedIn) {
        this.router.navigate(['login']);
        return false;
      }
    });

    return true;
  }

  canActivateChild(): boolean {
    this.auth.getPayload().subscribe(payload => {
      if (
        !(payload as any).permissions.some(x => {
          return x === 'is:admin';
        })
      ) {
        this.router.navigate(['/']);
        return false;
      }
    });
    return true;
  }
}
