export const environment = {
  production: false,
  client_details: {
    domain: 'jscebu.auth0.com',
    client_id: 'LbOSuxCGPSTxHpPr1uGKLNo6QkyO3X9y',
    redirect_uri: `https://angular.jscebu.org/login`,
    audience: 'user-api',
    scope: 'profile'
  },
  app_title: 'Angular Cebu'
};
